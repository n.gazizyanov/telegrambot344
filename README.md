# TelegramBot344
### Installation
1. Перед работой
```
npm install
npm build - чтобы сбилдить typescript и vue
npm start - запуск
```
2. Создать .env file по шаблону (взять адрес удаленного бд из вики)
3. Дополнительно:
```
npm watch - для того чтобы смотреть изменения в ts
npm watchv - для того чтобы vue
```
---

### Topology
1. `/dist` - Скомпиленный vue
2. `/generated` - Скомпиленный ts
3. `/public` - Статика для vue
4. `/src` - Исходный код ts
    2. `logic/bot` - логика бота
    3. `logic/db` - логика db
    3. `logic/srv` - логика сервера и апи
    3. `types` - типы для ts
5. `/srcFront` - Исходный код vue

### Heroku
1. `git remote add heroku` https://git.heroku.com/madbot909.git
2. `git push heroku` - отправить проект на хероку (там проходит команда npm build)