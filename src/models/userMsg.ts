import {Document, Schema, model} from "mongoose";

export enum MsgType{TYPE_AVAILABLE = 0, TYPE_LOOKFOR = 1}

export interface IUserMsg extends Document {
    name: string;
    username: string;
    from_id: number;
    date_modified: Date;
    msg_text: string;
    type: MsgType;
    doLog:()=>void
}

const UserMsgSchema = new Schema({
    name: { type: String, required: true},
    username: { type: String},
    from_id: { type: Number, required: true},
    date_modified: { type: Date, required: true},
    msg_text: { type: String, required: true},
    type: { type: Number, required: true},
});

UserMsgSchema.methods.doLog = function () {
    return `name: ${this.name}, username:${this.username}, from_id:${this.from_id}, date:${this.date_modified}, text:${this.msg_text}, type:${this.type}`;
};

export const UserMsgModel = model<IUserMsg>('userMsg', UserMsgSchema);