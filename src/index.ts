import * as dotenv from "dotenv";
import {init_db} from "./logic/db";
import {init_bot} from "./logic/bot";
import {init_srv} from "./logic/srv";

dotenv.config();

init_db(()=>{
    init_bot();
    init_srv();
});

