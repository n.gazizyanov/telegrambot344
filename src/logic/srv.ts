import express from "express";
import history from "connect-history-api-fallback";
import {MsgType, UserMsgModel} from "../models/userMsg";
const app = express();

export function init_srv(): void{
    const staticFileMiddleware = express.static('dist');
    app.get("/api/list/lookfor", (req, res) => {
        UserMsgModel.find({type:MsgType.TYPE_LOOKFOR}).lean().exec(function (err, users) {
            return res.end(JSON.stringify(users));
        })
    });
    app.get("/api/list/available", (req, res) => {
        UserMsgModel.find({type:MsgType.TYPE_AVAILABLE}).lean().exec(function (err, users) {
            return res.end(JSON.stringify(users));
        })
    });
    app.use(staticFileMiddleware);
    app.use(history({
        disableDotRule: true,
        verbose: true
    }));
    app.use(staticFileMiddleware);
    app.listen(process.env.PORT, () => console.log(`server is launched(${process.env.PORT})`))
}