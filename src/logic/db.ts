import {MongoError} from "mongodb";
const mongoose = require('mongoose');

export function init_db(onSuccess: () => void): void{

    mongoose.connect(process.env.DB_LINK, {useNewUrlParser: true}).then(()=>{
        console.log("db_success");
        onSuccess();
    }).catch((er: MongoError)=>{
        console.error("db_error", er)
    });
}