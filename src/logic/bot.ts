import Telegraf, {ContextMessageUpdate, Extra, Markup} from "telegraf";
import {IUserMsg, MsgType, UserMsgModel} from "../models/userMsg";
import {ExtraReplyMessage} from "telegraf/typings/telegram-types";
import {InlineQueryResultArticle, InlineQueryResultDocument} from "telegram-typings";
import * as path from "path";
import TelegrafI18n from "telegraf-i18n";
import {CronJob} from "cron";

const i18n = new TelegrafI18n({
    defaultLanguage: 'ru',
    allowMissing: false,
    directory: path.resolve(__dirname, '../locales'),
    useSession: true,
    sessionName: 'session'
});


export function init_bot(){
    const bot = new Telegraf(process.env.BOT_TOKEN);
    bot.use(i18n.middleware());
    bot.start((ctx: ContextMessageUpdate) => {
        const message = ctx.i18n.t('greeting', {
            username: ctx.from.username
        });
        return ctx.reply(message);
    });
    bot.help((ctx:ContextMessageUpdate) => ctx.reply(ctx.i18n.t('help')));
    bot.command('delete', ctx => {
        deleteUserMsgFromDb(ctx);
    });
    bot.command('debug_check', ctx => {
        deleteExpiredUserMsgFromDb()
    });

    bot.command('debug_panel', (ctx) => {
        return ctx.reply('<b>Debug panel</b>', Extra.HTML().markup((m: any) =>
            m.inlineKeyboard([
                m.callbackButton(ctx.i18n.t('debug-panel.check-expired'), 'check-expired'),
                m.callbackButton('test1', 'test1')
            ])))
    });
    bot.action('check-expired', (ctx) => {
        deleteExpiredUserMsgFromDb(1);
        ctx.answerCbQuery("")
    });
    bot.action('test1', (ctx) => {
        ctx.reply("test!");
        ctx.answerCbQuery("")
    });

    bot.use((ctx, next) => {
        return next().then(() => {
            if(ctx != null && ctx.message != null){
                console.log(`${ctx.message.message_id}: ${ctx.message.text} on ${ctx.message.date} from ${ctx.message.from.first_name} ${ctx.message.from.username} ${ctx.message.from.id}`)
            }
        })
    });

    bot.on('text', handleText);

    bot.launch().then(()=>{
        console.log("bot launched")
    }).catch((error)=>{
        console.error("launch error", {error})
    });

    function handleText(ctx:ContextMessageUpdate) {
        const text = ctx.message.text;
        checkHashTag(text, ["#available", "#доступен"], (hash)=>{
            saveUserMsgToDb(hash, ctx, MsgType.TYPE_AVAILABLE);
        });
        checkHashTag(text, ["#lookfor", "#ищу"], (hash)=>{
            saveUserMsgToDb(hash, ctx, MsgType.TYPE_LOOKFOR);
        });
    }

    function deleteUserMsgFromDb(ctx: ContextMessageUpdate) {
        UserMsgModel.deleteMany({from_id: ctx.message.from.id}).exec().then((m)=>{
            if(m.deletedCount > 0){
                ctx.reply(ctx.i18n.t('deleted'));
            } else{
                ctx.reply(ctx.i18n.t('deleted-not-found'))
            }
        })
    }
    function deleteExpiredUserMsgFromDb(time = 7 * 24 * 60 * 60) {
        const lte = new Date((new Date().getTime() - (time*1000)));
        console.log("checking expired..",{lte:lte});
        UserMsgModel.find(
        {
            "date_modified" : {
                $lte: lte,
            }
        }).distinct('from_id').exec().then((users: Array<number|string>)=>{
            users.forEach((from_id)=>{
                bot.telegram.sendMessage(from_id, i18n.t('ru','expired-deleted'));
            });
            UserMsgModel.deleteMany({
                "date_modified" : {
                    $lte: lte,
                }
            }).exec();
        })
    }

    function getTagName(type: MsgType) {
        switch (type){
            case MsgType.TYPE_LOOKFOR:
                return "#lookfor";
            case MsgType.TYPE_AVAILABLE:
                return "#available";
            default:
                return "";
        }
    }

    function saveUserMsgToDb(hash:string, ctx:ContextMessageUpdate, type:MsgType){
        UserMsgModel.deleteMany({from_id: ctx.message.from.id, type:type}).exec().then((m)=>{
            const item = new UserMsgModel({
                name: ctx.message.from.first_name + " " + ctx.message.from.last_name,
                username: ctx.message.from.username,
                from_id: ctx.message.from.id,
                date_modified: new Date(ctx.message.date*1000),
                msg_text: ctx.message.text,
                type: type,
            });
            item.save((err: any, item: IUserMsg)=>{
                if(err){
                    console.error(err);
                } else{
                    if(m.deletedCount > 0){
                        ctx.reply(ctx.i18n.t('item-modified',{
                            tag:getTagName(type)
                        }));
                    } else {
                        ctx.reply(ctx.i18n.t('item-created',{
                            tag:getTagName(type)
                        }));
                    }
                }
            })

        });
    }

    function checkHashTag(text:string, hashes:Array<string>, callback: (s:string)=>void): string{
        hashes.forEach((hash)=>{
            if(text.includes(hash+" ")){
                callback(hash);
                return hash;
            }
        });
        return null;
    }

    // Проверяем истекшие сообщения в 10:00 каждый день
    new CronJob('0 00 10 * * 1-7', function () {
        console.log('Check expired job');
        deleteExpiredUserMsgFromDb();
    }, null, true, 'Europe/Samara');
}