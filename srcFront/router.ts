import Vue from 'vue';
import Router from "vue-router";
const Home = ()=> import ( './views/Home.vue');
const LookForList = ()=>import ( './views/LookForList.vue');
const AvailableList = ()=>import ( './views/AvailableList.vue');
const About = ()=>import ( './views/About.vue');

Vue.use(Router);
export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/available',
            name: 'available',
            component: AvailableList
        },
        {
            path: '/lookfor',
            name: 'lookfor',
            component: LookForList
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
    ]
});
